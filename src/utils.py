import json
import requests
from bs4 import BeautifulSoup
from urllib.parse import urlparse


def display_tunnels(username, password, login_url, base_url, status_url):
    with requests.Session() as s:

        csrf = s.get(login_url)
        soup = BeautifulSoup(csrf.text, "html.parser")
        token = soup.find('input', {'name': 'csrf_token'})['value']

        payload = {
            'email': username,
            'password': password,
            'csrf_token': token
        }

        headers = {
            'Referer': base_url
        }

        p = s.post(login_url, data=payload, headers=headers)
        r = s.get(status_url)
        soup = BeautifulSoup(r.text, "html.parser")
        login_error = soup.find(class_="alert alert-error")
        preloaded = soup.find('div', {'id': 'preloaded'})['data-value']

        json_data = json.loads(preloaded)

        try:
            url_access = json_data['online_tunnels'][0]['url']
        except IndexError:
            print("Tunnel não encontrado!")
            exit(1)

        url = urlparse(url_access)

        print('Online Tunnels')
        print('---------------------------')
        print('Remote Address: {}'.format(json_data['online_tunnels'][0]['remote_addr']))
        print(u'Url: {}'.format(url.hostname))
        print(u'Port: {}'.format(url.port))

        url_hostname = url.hostname
        url_port = url.port

        return (url_hostname, url_port)
