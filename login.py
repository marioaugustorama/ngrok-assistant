#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import shlex
import subprocess

if sys.version_info<(3,5,0):
    sys.stderr.write("Você necessita de uma versão 3.5 ou superior!\n")
    exit(1)

import argparse
from configparser import ConfigParser

from src.utils import display_tunnels

base_url = 'https://dashboard.ngrok.com'
login_url = 'https://dashboard.ngrok.com/user/login'
status_url = 'https://dashboard.ngrok.com/status'

config = ConfigParser()
config.read('login.cfg')
username = config.get('login', 'username')
password = config.get('login', 'password')


def main():
    ap = argparse.ArgumentParser()
    ap.add_argument("-s", "--ssh", action="store_true", help=u"Abre ssh no endereço de destino.")
    ap.add_argument("-d", "--display", action="store_true", help=u"Mostra os túneis abertos disponíveis.")
    args = vars(ap.parse_args())

    if len(sys.argv) == 1:
        ap.print_help()
        sys.exit()

    if args["display"]:
        display_tunnels(username, password, login_url, base_url, status_url)
    elif args["ssh"]:
        connect = display_tunnels(username, password, login_url, base_url, status_url)
        command = "ssh {} -p {}".format(connect[0], connect[1])
        args = shlex.split(command)
        try:
            ssh = subprocess.run(args)
        except KeyboardInterrupt:
            sys.exit(0);

if __name__ == "__main__":
    main()
