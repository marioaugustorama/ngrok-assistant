# Ngrok Assistant
### Assistente para conexão e informações de tunneis ngrok.

####  Configuração:

$ cp login.cfg.dist login.cfg

Abra o arquivo login.cfg e preencha com seus dados de acesso.

#### Uso: 
$ ./login.py

    Online Tunnels
    ---------------------------
    Remote Address: XXX.XXX.XXX.XXX
    Url: tcp://0.tcp.ngrok.io:13319
    Port: 13319

