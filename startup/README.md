# Installation 


Step 1: Place [`ngrok`](https://ngrok.com/download) in `/usr/local/bin/`

Step 2: Get `authtoken` from ngrok website, then add it to `/etc/ngrok/ngrok.yml`

Step 3: Add `ngrok.service` to `/etc/systemd/system/`

Step 4: Start ngrok service by typing:
```
    systemctl daemon-reload
    systemctl enable ngrok.service
    systemctl start ngrok.service
```
